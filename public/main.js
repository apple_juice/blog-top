"use strict";

window.addEventListener('DOMContentLoaded', function () {
  // START OF: is mobile =====
  function isMobile() {
    return (/Android|iPhone|iPad|iPod|BlackBerry/i).test(navigator.userAgent || navigator.vendor || window.opera);
  }
  // ===== END OF: is mobile

  const BODY = $('body');

  // START OF: mark is mobile =====
  (function() {
    BODY.addClass('loaded');

    if(isMobile()){
      BODY.addClass('body--mobile');
    }else{
      BODY.addClass('body--desktop');
    }
  })();
  // ===== END OF: mark is mobile

  // START OF: loader =====
  var loader = (function(){
    var $introImagesSection = $('.js-image-section');
    var $body = $('body');
    var durations = {
      revealIntro: 50, //taken from CSS
      showIntroSlider: 1000,
      showIntroSlides: 1000,
      introSlidesShift: 1500,
      percentFakeIncrementing: 1000
    };

    function revealIntro() {
      $body.addClass('page-loaded');
    }

    function init(){
      $introImagesSection.imagesLoaded({ background: false })
      //reveal the into once all the images loaded
        .always( function( instance ) {
          setTimeout(function(){
            revealIntro();
          }, durations.percentFakeIncrementing);
        })
        .done( function( instance ) {
          console.log('All images successfully loaded.');
        })
        .fail( function() {
          console.log('Images loaded, at least one is broken.');
        })
        .progress( function( instance, image ) {
        });
    }
    return {
      revealIntro: revealIntro,
      init: init
    }
  }());

  loader.init();

  let fullpage = (function () {
    function init(){
      let arrayOfIDs = $('[data-anchor]').map(function() { return $(this).data('anchor') }).get();
      let slideTimeout;

      $('#fullpage').fullpage({
        anchors: arrayOfIDs,
        keyboardScrolling: true,
        menu: '#menu',
        responsiveWidth: 769,
        css3: true,
        easingcss3: 'ease',
        scrollingSpeed: 700,
        navigation: false,
        slidesNavigation: false,
        controlArrows: false,
        verticalCentered: false,
        dragAndMove: 'horizontal',
        fitToSection: false,
        resetSliders: true,
        licenseKey: '93FA04FE-89684C69-96ABC1F4-C5DCF642',
        dragAndMoveKey: 'YmxvZ2FuZHRvcC5jb21fSzd2WkhKaFowRnVaRTF2ZG1VPWxXeA==',
        resetSlidersKey: 'YmxvZ2FuZHRvcC5jb21fRU1XY21WelpYUlRiR2xrWlhKejBVQw==',
        fixedElements: '.header, .footer',
        normalScrollElements: '.modal, .navbar-collapse',
        bigSectionsDestination: 'top',

        afterLoad: function(index){
          $('.fp-section').each(function () {
            let totalSlides = $(this).find('.slide').length
            $(this).find('.slider_counter__current').text('01')
            $(this).find('.slider_counter__total').text(totalSlides)
          })

          if (!isMobile()) {
            clearInterval(slideTimeout);

            slideTimeout = setInterval(function () {
              $.fn.fullpage.moveSlideRight();
            }, 10000);
          }
        },

        onLeave: function(index, origin, destination, direction){
          BODY.removeClass('menu-opened')
          $('._open_menu').removeClass('active')
          $('.sidebar').removeClass('sidebar--show');
          $('.navbar-collapse').collapse('hide');
          if (!isMobile()) {
            clearInterval(slideTimeout);
          }
        },

        afterSlideLoad: function (origin, destination, direction) {
          let current = fullpage_api.getActiveSlide().index + 1

          if (current <= 9) {
            $('.fp-section.active').find('.slider_counter__current').text('0' + current)
          } else {
            $('.fp-section.active').find('.slider_counter__current').text(current)
          }

          if (!isMobile()) {
            clearInterval(slideTimeout);

            slideTimeout = setInterval(function () {
              $.fn.fullpage.moveSlideRight();
            }, 10000);
          }
        },

        onSlideLeave: function () {
          if (!isMobile()) {
            clearInterval(slideTimeout);
          }
        }

      });
    }
    return {
      init: init
    }
  }());

  fullpage.init();

  $(document).ready(function() {
    Splitting();
    $(this).scrollTop(0);
  });

/*
  BODY.mousemove(function (e) {
    $(this).addClass('custom-cursor');
    $(".mouse").show().css({
      "left": e.clientX,
      "top": e.clientY
    });

    $('a, button').hover(
      function(){ $(".mouse").addClass('mouse--hover') },
      function(){ $(".mouse").removeClass('mouse--hover') }
    );
  }).mouseout(function () {
    $(".cursor").hide();
    BODY.removeClass('custom-cursor')
  });
*/

  $('[data-clear-form]').on('click', function () {
    let target = $(this).data('clear-form')
    $(target)[0].reset();
  })

  var form = $('#contact_form');

  $(form).validate({
    phone: {
      requiredPhone: true,
      minlengthPhone: 10
    },
    submitHandler: function(form) {
      var name, subject, email, message;

      name = $('input[name=name]').val();
      subject = $('input[name=subject]').val();
      email = $('input[name=email]').val();
      message = $('textarea[name=message]').val();

      $.ajax({
        url: form.action,
        type: form.method,
        data: {name: name, subject: subject, message:message, email: email },
        success: function(response) {
          $('#contact_form').addClass('form-success');
          $('#contact_form')[0].reset();
        }
      });
    }
  });
});